'use strict';

define('cw2-connect4/tests/app.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | app');

  QUnit.test('app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'app.js should pass ESLint\n\n');
  });

  QUnit.test('components/connect-4.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'components/connect-4.js should pass ESLint\n\n117:9 - \'draw\' is assigned a value but never used. (no-unused-vars)\n327:9 - \'createjs\' is not defined. (no-undef)\n328:9 - \'createjs\' is not defined. (no-undef)\n329:9 - \'createjs\' is not defined. (no-undef)\n333:25 - \'createjs\' is not defined. (no-undef)\n336:25 - \'createjs\' is not defined. (no-undef)\n371:35 - \'createjs\' is not defined. (no-undef)\n381:34 - \'createjs\' is not defined. (no-undef)\n393:9 - \'createjs\' is not defined. (no-undef)\n415:21 - \'createjs\' is not defined. (no-undef)\n467:17 - \'createjs\' is not defined. (no-undef)\n479:13 - \'createjs\' is not defined. (no-undef)\n482:13 - \'createjs\' is not defined. (no-undef)');
  });

  QUnit.test('resolver.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'resolver.js should pass ESLint\n\n');
  });

  QUnit.test('router.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'router.js should pass ESLint\n\n');
  });

  QUnit.test('routes/game.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/game.js should pass ESLint\n\n');
  });

  QUnit.test('routes/instruction.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/instruction.js should pass ESLint\n\n');
  });
});
define('cw2-connect4/tests/helpers/destroy-app', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = destroyApp;
  function destroyApp(application) {
    Ember.run(application, 'destroy');
  }
});
define('cw2-connect4/tests/helpers/module-for-acceptance', ['exports', 'qunit', 'cw2-connect4/tests/helpers/start-app', 'cw2-connect4/tests/helpers/destroy-app'], function (exports, _qunit, _startApp, _destroyApp) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  exports.default = function (name) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    (0, _qunit.module)(name, {
      beforeEach: function beforeEach() {
        this.application = (0, _startApp.default)();

        if (options.beforeEach) {
          return options.beforeEach.apply(this, arguments);
        }
      },
      afterEach: function afterEach() {
        var _this = this;

        var afterEach = options.afterEach && options.afterEach.apply(this, arguments);
        return Ember.RSVP.resolve(afterEach).then(function () {
          return (0, _destroyApp.default)(_this.application);
        });
      }
    });
  };
});
define('cw2-connect4/tests/helpers/start-app', ['exports', 'cw2-connect4/app', 'cw2-connect4/config/environment'], function (exports, _app, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = startApp;
  function startApp(attrs) {
    var attributes = Ember.merge({}, _environment.default.APP);
    attributes.autoboot = true;
    attributes = Ember.merge(attributes, attrs); // use defaults, but you can override;

    return Ember.run(function () {
      var application = _app.default.create(attributes);
      application.setupForTesting();
      application.injectTestHelpers();
      return application;
    });
  }
});
define('cw2-connect4/tests/integration/components/connect-4-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForComponent)('connect-4', 'Integration | Component | connect 4', {
    integration: true
  });

  (0, _emberQunit.test)('it renders', function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template({
      "id": "fXe8hnYp",
      "block": "{\"symbols\":[],\"statements\":[[1,[18,\"connect-4\"],false]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template({
      "id": "sFbgxBHG",
      "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"connect-4\",null,null,{\"statements\":[[0,\"      template block text\\n\"]],\"parameters\":[]},null],[0,\"  \"]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), 'template block text');
  });
});
define('cw2-connect4/tests/test-helper', ['cw2-connect4/app', 'cw2-connect4/config/environment', '@ember/test-helpers', 'ember-qunit'], function (_app, _environment, _testHelpers, _emberQunit) {
  'use strict';

  (0, _testHelpers.setApplication)(_app.default.create(_environment.default.APP));

  (0, _emberQunit.start)();
});
define('cw2-connect4/tests/tests.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | tests');

  QUnit.test('helpers/destroy-app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/destroy-app.js should pass ESLint\n\n');
  });

  QUnit.test('helpers/module-for-acceptance.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/module-for-acceptance.js should pass ESLint\n\n');
  });

  QUnit.test('helpers/start-app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/start-app.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/connect-4-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/connect-4-test.js should pass ESLint\n\n');
  });

  QUnit.test('test-helper.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'test-helper.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/game-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/game-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/instruction-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/instruction-test.js should pass ESLint\n\n');
  });
});
define('cw2-connect4/tests/unit/routes/game-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('route:game', 'Unit | Route | game', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var route = this.subject();
    assert.ok(route);
  });
});
define('cw2-connect4/tests/unit/routes/instruction-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('route:instruction', 'Unit | Route | instruction', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var route = this.subject();
    assert.ok(route);
  });
});
require('cw2-connect4/tests/test-helper');
EmberENV.TESTS_FILE_LOADED = true;
//# sourceMappingURL=tests.map
